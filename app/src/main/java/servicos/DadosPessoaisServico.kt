package servicos

import basica.DadosPessoais
import retrofit2.Call
import retrofit2.http.GET

interface DadosPessoaisServico {
    @GET("r3xig")
    fun trazerDadosPessoais(): Call<DadosPessoais>
}