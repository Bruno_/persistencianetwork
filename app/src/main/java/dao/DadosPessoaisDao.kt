package dao

import android.arch.persistence.room.*
import basica.DadosPessoais

@Dao
interface DadosPessoaisDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun replaceDadosPessoais(dadosPessoais: DadosPessoais?)

    @Delete
    fun removerDadosPessoais(dadosPessoais: DadosPessoais?)

    @Query("SELECT * FROM DadosPessoais")
    fun getDadosPessoais(): DadosPessoais


}