package basica

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class DadosPessoais(@PrimaryKey val id: Long, val nome: String?, val idade: Int,val rg: Int) {


}