package bruno_ultra.com.br.projectpersistencianetwork

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import basica.DadosPessoais
import dao.DadosPessoaisDao

@Database(entities = arrayOf(DadosPessoais::class),version = 1,exportSchema = false)
abstract class BancoDeDados : RoomDatabase() {

    abstract fun dadosPessoaisDao(): DadosPessoaisDao

}