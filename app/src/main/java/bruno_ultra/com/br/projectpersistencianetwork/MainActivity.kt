package bruno_ultra.com.br.projectpersistencianetwork

import android.arch.persistence.room.Room
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import basica.DadosPessoais
import kotlinx.android.synthetic.main.activity_main.*
import retrofit.BuilderRetrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import util.Comum


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       val  bd = Room.databaseBuilder(this,BancoDeDados::class.java,
                "dadosPessoais_bd").allowMainThreadQueries().build()

        val callTrazerDados = object: Callback<DadosPessoais>{
            override fun onFailure(call: Call<DadosPessoais>?, t: Throwable?) {
                Toast.makeText(this@MainActivity,getString(R.string.msg_error),Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<DadosPessoais>?, response: Response<DadosPessoais>?) {
               response?.let {
                   inserindoInformacoesDadosNaTela(it.body())
                   bd.dadosPessoaisDao().replaceDadosPessoais(it.body())
               }

            }

        }
        if(Comum.veriricarNetwork(this)){
            BuilderRetrofit.trazerDadosPessoais(callTrazerDados)
            
        }else{
            val dadosPessoais = bd.dadosPessoaisDao().getDadosPessoais()
            inserindoInformacoesDadosNaTela(dadosPessoais)

        }

    }

    fun inserindoInformacoesDadosNaTela(dadosPessoais: DadosPessoais?){
        dadosPessoais?.let {
            txvNome.setText(it.nome)
            txvIdade.setText(it.idade.toString())
            txvRg.setText(it.rg.toString())
        }
    }
}
