package retrofit

import basica.DadosPessoais
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import servicos.DadosPessoaisServico

class BuilderRetrofit(){

   companion object {
            val retrofit = Retrofit.Builder()
                   .baseUrl("https://api.myjson.com/bins/")
                   .addConverterFactory(GsonConverterFactory.create())
                   .build()


       val dadosPessoaisServico = retrofit.create(DadosPessoaisServico::class.java)
       fun trazerDadosPessoais (call: Callback<DadosPessoais>) {
           dadosPessoaisServico.trazerDadosPessoais().enqueue(call)
       }

   }
}